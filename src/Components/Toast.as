/*
	Copyright (c) 2011 http://www.immanuelnoel.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in
	the Software without restriction, including without limitation the rights to
	use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
	of the Software, and to permit persons to whom the Software is furnished to do
	so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.
*/
package Components
{
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import spark.components.BorderContainer;
	import spark.components.Label;
	import spark.components.View;
	import spark.layouts.VerticalLayout;
	
	public class Toast extends BorderContainer
	{
		/**
		 *  CONSTRUCTER
		 */
		public function Toast(context:Object, textToDisplay:String, timeOut:Number = 3000, width:Number = 290, height:Number = 40, color:String = '#454545', x:Number = NaN, y:Number = NaN):void
		{
			super();
			
			// TIMER TO 'REMOVE' TOAST AFTER THE SPECIFIED 'TIMEOUT'
			var timer:Timer = new Timer(timeOut, 1);
			timer.addEventListener(TimerEvent.TIMER_COMPLETE, function(event:TimerEvent):void{
				context.removeElementAt(context.numElements - 1);
			});
			timer.start();
			
			createUI(textToDisplay, color, width, height);
			
			// TOAST POSITION
			positionToast(context);
		}
		
		/**
		 *  CREATE TOAST UI COMPONENTS
		 */
		private function createUI(textToDisplay:String, color:String, width:Number, height:Number):void
		{
			selfProperties(color, width, height);
			addText(textToDisplay);
		}
		
		/**
		 * ASSIGNING ATTRIBUTES TO THE BASE CONTAINER
		 */
		private function selfProperties(color:String, width:Number, height:Number):void
		{
			this.width=width;
			this.height=height;
			this.setStyle('backgroundAlpha','0.75');
			this.setStyle('backgroundColor',color);
			this.setStyle('borderColor','#242424');
			this.setStyle('borderWeight','2');
			this.setStyle('cornerRadius','7');
			
			var vl:VerticalLayout = new VerticalLayout();
			vl.horizontalAlign = "center";
			vl.verticalAlign = "middle";
			this.layout = vl;
		}
		
		/**
		 *  CREATE LABEL
		 */
		private function addText(textToDisplay:String):void
		{
			var lbl:Label = new Label();
			lbl.setStyle('color','#EDEDED');
			lbl.setStyle('fontSize','18');
			lbl.text = textToDisplay;
			lbl.setStyle('textAlign', 'center');
			lbl.width = this.width;
			
			this.addElement(lbl);
		}
		
		/**
		 *  POSITIONING THE TOAST 
		 */
		private function positionToast(context:Object):void
		{
			// POSITION TOAST 
			if(Number(x) && Number(y))
			{
				this.x = x;
				this.y = y;
			}
			else
			{
				if(Number(x) && !Number(y))
				{
					throw new Error("Toast : 'Y' attribute also needs to be specified");
				}
				
				this.x = (context.width / 2) - (this.width / 2);
				this.y = (context.height * 3 / 4) - (this.height / 2);
			}
			
			// IF PREVIOUSLY ADDED COMPONENT IS A TOAST, RE-ORDER TO MANAGE TIMER
			if(context.getElementAt(context.numElements - 1) is Toast)
				context.addElementAt(this, context.numElements - 1);
			else
				context.addElement(this);
		}
	}
}