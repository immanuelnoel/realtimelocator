/*
	Copyright (c) 2011 http://www.immanuelnoel.com
	
	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in
	the Software without restriction, including without limitation the rights to
	use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
	of the Software, and to permit persons to whom the Software is furnished to do
	so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.
*/
package controller 
{
	import Components.Toast;
	import Event.LocationEvent;
	import flash.events.NetStatusEvent;
	import flash.net.GroupSpecifier;
	import flash.net.NetConnection;
	import flash.net.NetGroup;
	import flash.net.NetGroupReplicationStrategy;
	import flash.net.NetGroupSendResult;
	import mx.collections.ArrayCollection;
	import mx.core.FlexGlobals;
	
	/**
	 * 
	 * p2p - Singleton class to handle P2P connections
	 * 
	*/
	[Bindable]
	public class p2p
	{
		public static var _p2pQueue:p2p = null;
		
		/**
		 * Method to maintain a singleton instance
		 */
		public static function getInstance():p2p
		{
			if(_p2pQueue == null)
			{
				_p2pQueue = new p2p();
			}
			return _p2pQueue;
		}
		
		/**
		 * RTMFP Capable Server URL <br>
		 * Eg. Cirrus : rtmfp://p2p.rtmfp.net/
		 */
		private const SERVER_ADDRESS:String = "rtmfp://p2p.rtmfp.net/";
		
		/**
		 * Developer Key (In case of Cirrus) <br>
		 * Register for a developer key at http://labs.adobe.com/technologies/cirrus
		 */
		private const DEVELOPER_KEY:String = "YOUR-CIRRUS-DEVELOPER-KEY-HERE";
		
		/**
		 * NetConnection Object
		 */
		private var netConnection:NetConnection;
		
		/**
		 * NetGroup Object
		 */
		private var netGroup:NetGroup;
		
		/**
		 * Method to setup NetConnection
		 */
		public function init():void
		{
			netConnection = new NetConnection();
			netConnection.addEventListener(NetStatusEvent.NET_STATUS, netConnectionHandler);
			
			// PARAMETERS : CIRRUS CREDENTIALS ('rtmfp://' FOR ISOLATED LAN)
			netConnection.connect(SERVER_ADDRESS+DEVELOPER_KEY);
		}
		
		/**
		 * Method to setup NetGroup
		 */
		private function setupGroup():void
		{
			var groupspec:GroupSpecifier = new GroupSpecifier("com.inoel.RealTimeLocator");
			groupspec.ipMulticastMemberUpdatesEnabled = true;
			groupspec.multicastEnabled = true;
			groupspec.routingEnabled = true;
			groupspec.postingEnabled = true;
			groupspec.serverChannelEnabled = true;
			groupspec.objectReplicationEnabled = true;
			
			groupspec.addIPMulticastAddress("239.254.254.2:3030");
			
			netGroup = new NetGroup(netConnection, groupspec.groupspecWithAuthorizations());
			netGroup.addEventListener(NetStatusEvent.NET_STATUS, netConnectionHandler);
		}
		
		/**
		 * Method to handle incoming NetStatus events
		 */
		public function netConnectionHandler(e:NetStatusEvent):void
		{
			switch(e.info.code)
			{
				case "NetConnection.Connect.Success":
					trace("NetConnection established");
					setupGroup();
					break;
				
				case "NetConnection.Connect.Closed":
					trace("NetConnection Closed for Restart");
					init();
					break;
				
				case "NetGroup.Connect.Success":
					trace("NetGroup setup");
					netGroup.replicationStrategy = NetGroupReplicationStrategy.LOWEST_FIRST;
					new Toast(FlexGlobals.topLevelApplication.navigator, "Connected to NetGroup");
					break;
				
				case "NetGroup.Connect.Closed":
					trace("NetGroup Closed for Restart");
					netConnection.close();
					break;
				
				case "NetGroup.Neighbor.Connect":
					// Neighbor Connected
					trace("Neighbor Connected " + e.info.peerID);
					break;
				
				case "NetGroup.Neighbor.Disconnect":
					//Neighbor Disconnected
					trace("Neighbor Disconnected " + e.info.peerID);
					break;
				
				case "NetGroup.Posting.Notify":
					// DATA RECEIVED
					trace("Incoming Notification : " + e.info.message.peerId);
					
					if(e.info.message)
					{
						// CONSTRUCT TEMP OBJECT FROM RECEIVED DATA
						var obj:Object = new Object();
						obj.latitude = e.info.message.latitude;
						obj.longitude = e.info.message.longitude;
						obj.speed = e.info.message.speed;
						obj.selfLocation = false;
						obj.peerId = e.info.message.peerId;
						
						// DISPATCH LocationEvent.LocationReceived EVENT - CAUGHT IN MapView
						FlexGlobals.topLevelApplication.dispatchEvent(new LocationEvent(LocationEvent.LocationReceived, obj, false, true));
					}
					break;
				
				default:
					trace(e.info.code);
					break;
			}
		}
		
		/**
		 * Method to post data to other peers over the air 
		 */
		public function share(currentLocation:Object):void
		{
			// IF > 1 MEMBERS EXIST IN GROUP, POST DATA (LOCATION)
			if(netGroup && netGroup.estimatedMemberCount > 0)
			{
				var sendObject:Object = new Object();
				sendObject.latitude = currentLocation.latitude;
				sendObject.longitude = currentLocation.longitude;
				sendObject.speed = currentLocation.speed;
				sendObject.selfLocation = currentLocation.selfLocation;
				
				// CHANGE PEERID FROM 'SELF' TO RECOGNIZABLE PEER ID
				sendObject.peerId = netConnection.nearID;
				
				netGroup.post(sendObject);
			}
		}
	}
}